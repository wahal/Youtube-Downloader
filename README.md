<h3>Youtube to MP3 Downloader</h3>
A python project to download MP3 files from youtube videos.

NOTE: Requires dependency of an external module 'youtube_dl'.
Download from pip using the command:

<i>pip install youtube_dl</i>

<h5>Developed & Maintained by Mrinal Wahal</h5>
